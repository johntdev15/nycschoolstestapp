package com.example.a20230301_jonathantorres_nycschools;

import androidx.annotation.NonNull;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.a20230301_jonathantorres_nycschools.model.SAT;
import com.example.a20230301_jonathantorres_nycschools.network.RetroInstance;
import com.example.a20230301_jonathantorres_nycschools.network.RetroService;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

@RunWith(AndroidJUnit4.class)
public class NetworkTest {

    //Verify if the network call has successfully launched
    ///With more time, I could have added responses and valid API

    @Test
    public void testPostsResponse(){

        Retrofit retrofit = RetroInstance.getRetrofit();
        RetroService retroService = retrofit.create(RetroService.class);
        Call<List<SAT>> call = retroService.getSAT("01M292");

        call.enqueue(new Callback<List<SAT>>() {
            @Override
            public void onResponse(@NonNull Call<List<SAT>> call, @NonNull Response<List<SAT>> response) {
                assert response.isSuccessful();
                if (response.body() != null){
                    List<SAT> posts = response.body();
                    assert posts.size() != 0;
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<SAT>> call, @NonNull Throwable t) {

            }
        });
    }

}
