package com.example.a20230301_jonathantorres_nycschools.viewmodel;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.a20230301_jonathantorres_nycschools.model.SAT;
import com.example.a20230301_jonathantorres_nycschools.network.RetroInstance;
import com.example.a20230301_jonathantorres_nycschools.network.RetroService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class DetailsViewModel extends ViewModel {

    public MutableLiveData<SAT> satScores = new MutableLiveData<>();

    //Retrofit call to pull sAT Data from API using the school's current dbn ID
    public void getSchoolSatScoreData(String dbn, Context context){

        Retrofit retrofit = RetroInstance.getRetrofit();
        RetroService retroService = retrofit.create(RetroService.class);
        Call<List<SAT>> call = retroService.getSAT(dbn);

        call.enqueue(new Callback<List<SAT>>() {
            @Override
            public void onResponse(Call<List<SAT>> call, Response<List<SAT>> response) {
                if (!response.isSuccessful()){
                    Toast.makeText(context, "Problem accessing to the server, Please try again later",
                            Toast.LENGTH_SHORT).show();
                }else{
                    List<SAT> posts = response.body();
                    if (posts != null){
                        if (posts.size() != 0){
                            for (SAT sat : posts) {

                                sat = new SAT(sat.getDbn(), sat.getNum_of_sat_test_takers(),
                                        sat.getSat_critical_reading_avg_score(),
                                        sat.getSat_math_avg_score(),
                                        sat.getSat_writing_avg_score());
                                Log.d("whatsInSat",posts.toString());
                                satScores.setValue(sat);
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<List<SAT>> call, Throwable t) {
                Log.d("Something went wrong", t.toString());
                Toast.makeText(context, "Problem connecting to the internet, Please try again",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    public MutableLiveData<SAT> getSatScoresData(){
        return satScores;
    }

    public void startGoogleMaps(Context context, String latLog){
        Uri gmmIntentUri = Uri.parse("geo:" + latLog);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        context.startActivity(mapIntent);
    }
}
