package com.example.a20230301_jonathantorres_nycschools.viewmodel;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.example.a20230301_jonathantorres_nycschools.model.School;
import com.example.a20230301_jonathantorres_nycschools.network.RetroInstance;
import com.example.a20230301_jonathantorres_nycschools.network.RetroService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SearchViewModel extends androidx.lifecycle.ViewModel {

    public MutableLiveData<ArrayList<School>> schoolMutableLiveData = new MutableLiveData<>();

    //Retrofit call to pull School Data from API
    public void getSchoolData(Context context){

        ArrayList<School> listOfSchools = new ArrayList<>();
        Retrofit retrofit = RetroInstance.getRetrofit();
        RetroService retroService = retrofit.create(RetroService.class);
        Call<List<School>> call = retroService.getSchools();

        call.enqueue(new Callback<List<School>>() {
            @Override
            public void onResponse(@NonNull Call<List<School>> call, @NonNull Response<List<School>> response) {
                if (!response.isSuccessful()){
                    Toast.makeText(context, "Problem accessing to the server, Please try again later",
                            Toast.LENGTH_SHORT).show();
                }else{
                    List<School> posts = response.body();
                    if(posts != null){
                        for (School school : posts) {
                            listOfSchools.add(new School(school.getDbn(), school.getSchool_name(),
                                    school.getOverview_paragraph(), school.getPhone_number(),
                                    school.getSchool_email(), school.getWebsite(), school.getGrades(),
                                    school.getLocation()));
                        }
                        schoolMutableLiveData.setValue(listOfSchools);
                    }
                }
            }
            @Override
            public void onFailure(@NonNull Call<List<School>> call, Throwable t) {
                Log.d("Something went wrong", t.toString());
                Toast.makeText(context, "Problem connecting to the internet, Please try again",
                        Toast.LENGTH_SHORT).show();
            }
        });

    }


    public MutableLiveData<ArrayList<School>> getSchoolList(){
        return schoolMutableLiveData;
    }


}
