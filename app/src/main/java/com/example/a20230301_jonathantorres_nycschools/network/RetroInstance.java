package com.example.a20230301_jonathantorres_nycschools.network;

import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

//Retrofit Instance class to set base Url to download data
//For more info: https://data.cityofnewyork.us/Education/DOE-High-School-Directory-2017/s3k6-pzi2
public class RetroInstance {

    public static final String baseUrl = "https://data.cityofnewyork.us/resource/";

    public static Retrofit getRetrofit(){

        return new Retrofit.Builder().baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create()).build();
    }
}
