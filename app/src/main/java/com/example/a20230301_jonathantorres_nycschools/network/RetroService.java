package com.example.a20230301_jonathantorres_nycschools.network;

import com.example.a20230301_jonathantorres_nycschools.model.SAT;
import com.example.a20230301_jonathantorres_nycschools.model.School;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

//Interface for Retrofit network calls
public interface RetroService {

    @GET("s3k6-pzi2.json?")
    Call<List<School>> getSchools();

    @GET("f9bf-2cp4.json")
    Call<List<SAT>> getSAT(@Query("dbn") String dbn);

}
