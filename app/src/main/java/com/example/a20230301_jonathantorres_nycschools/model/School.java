package com.example.a20230301_jonathantorres_nycschools.model;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

//Model Data for the School

//Could have implemented Parcelable here for better performance
public class School implements Serializable {

    @NotNull
        String dbn;
    @NotNull
        String school_name;

    String overview_paragraph;
        String phone_number;
        String school_email;
        String website;
        String grades;
        String location;

    public School(@NonNull String _id, @NonNull String _name, String _overview_paragraph, String _phoneNumber, String _email, String _website,
                  String _grades, String _location){

        dbn = _id;
        school_name = _name;
        overview_paragraph = _overview_paragraph;
        phone_number = _phoneNumber;
        school_email = _email;
        website =_website;
        grades = _grades;
        location = _location;
    }

    @NonNull
    public String getDbn() {
        return dbn;
    }

    @NonNull
    public String getSchool_name() {
        return school_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getOverview_paragraph() {
        return overview_paragraph;
    }

    public String getSchool_email() {
        return school_email;
    }

    public String getWebsite() {
        return website;
    }

    public String getGrades() {
        return grades;
    }

    public String getLocation() {
        return location;
    }

}
