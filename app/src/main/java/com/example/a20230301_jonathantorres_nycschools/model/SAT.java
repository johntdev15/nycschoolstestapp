package com.example.a20230301_jonathantorres_nycschools.model;

import org.jetbrains.annotations.NotNull;

//Model Data for the SAT Scores
public class SAT {

    @NotNull
    String dbn;
    Integer num_of_sat_test_takers;
    Integer sat_critical_reading_avg_score;
    Integer sat_math_avg_score;
    Integer sat_writing_avg_score;

    public SAT(String _dbn, Integer _numbOfSatTestTakers, Integer _criticalReading, Integer _math,
               Integer _writing){
        dbn = _dbn;
        num_of_sat_test_takers = _numbOfSatTestTakers;
        sat_critical_reading_avg_score = _criticalReading;
        sat_math_avg_score = _math;
        sat_writing_avg_score = _writing;

    }

    @NotNull
    public String getDbn() {
        return dbn;
    }

    public void setDbn(@NotNull String dbn) {
        this.dbn = dbn;
    }

    public Integer getNum_of_sat_test_takers() {
        return num_of_sat_test_takers;
    }

    public void setNum_of_sat_test_takers(Integer num_of_sat_test_takers) {
        this.num_of_sat_test_takers = num_of_sat_test_takers;
    }

    public Integer getSat_critical_reading_avg_score() {
        return sat_critical_reading_avg_score;
    }

    public void setSat_critical_reading_avg_score(Integer sat_critical_reading_avg_score) {
        this.sat_critical_reading_avg_score = sat_critical_reading_avg_score;
    }

    public Integer getSat_math_avg_score() {
        return sat_math_avg_score;
    }

    public void setSat_math_avg_score(Integer sat_math_avg_score) {
        this.sat_math_avg_score = sat_math_avg_score;
    }

    public Integer getSat_writing_avg_score() {
        return sat_writing_avg_score;
    }

    public void setSat_writing_avg_score(Integer sat_writing_avg_score) {
        this.sat_writing_avg_score = sat_writing_avg_score;
    }
}
