package com.example.a20230301_jonathantorres_nycschools.ui;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.example.a20230301_jonathantorres_nycschools.R;
import com.example.a20230301_jonathantorres_nycschools.adapter.SearchAdapter;
import com.example.a20230301_jonathantorres_nycschools.viewmodel.SearchViewModel;
import com.example.a20230301_jonathantorres_nycschools.model.School;

import java.util.ArrayList;

public class SearchFragment extends Fragment {

    ArrayList<School> listOfSchools = new ArrayList<>();
    public static SearchViewModel searchViewModel = new SearchViewModel();
    private RecyclerView searchList_RV;
    public SearchAdapter searchAdapter;
    private ProgressBar searchProgress_PB;
    public EditText search_ET;
    public SearchFragment() {
        // Required empty public constructor
    }

    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View searchView = inflater.inflate(R.layout.search_screen_fragmemt, container, false);
        // Inflate the layout for this fragment
        searchList_RV = (RecyclerView)searchView.findViewById(R.id.search_list_RV);
        searchList_RV.setLayoutManager(new LinearLayoutManager(getContext()));
        search_ET = (EditText)searchView.findViewById(R.id.search_ET);
        searchProgress_PB = (ProgressBar)searchView.findViewById(R.id.search_progress_PB);
        return searchView;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadSchoolsData();
    }

    //Pulls Data from the network and sets the adapter
    public void loadSchoolsData(){

        if(getActivity() != null){
            searchViewModel = new ViewModelProvider(getActivity()).get(SearchViewModel.class);
            searchViewModel.getSchoolList().observe(getActivity(), new Observer<ArrayList<School>>() {
                @Override
                public void onChanged(ArrayList<School> list) {
                    listOfSchools = list;
                    searchAdapter = new SearchAdapter(getContext(), listOfSchools);
                    searchList_RV.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));
                    searchList_RV.setAdapter(searchAdapter);
                    searchAdapter.notifyDataSetChanged();
                    searchProgress_PB.setVisibility(View.GONE);
                }
            });
            searchViewModel.getSchoolData(getContext());
        }


    }

}