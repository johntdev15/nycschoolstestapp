package com.example.a20230301_jonathantorres_nycschools.ui;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a20230301_jonathantorres_nycschools.R;
import com.example.a20230301_jonathantorres_nycschools.model.SAT;
import com.example.a20230301_jonathantorres_nycschools.model.School;
import com.example.a20230301_jonathantorres_nycschools.viewmodel.DetailsViewModel;

import java.util.Locale;

public class DetailsFragment extends Fragment {

    SAT schoolSAT;
    School school;
    private static final String ARG_PARAM1 = "param1";

    //Declare Views
    private static DetailsViewModel detailsViewModel = new DetailsViewModel();

    TextView name_TV, overview_TV, phoneNumber_TV, email_TV, website_TV, grades_TV, location_TV;

    TextView sat_critical_TV, sat_math_TV, sat_writing_TV, sat_title_TV;

    public DetailsFragment() {
        // Required empty public constructor
    }

    public static DetailsFragment newInstance(School school) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, school);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            school = (School)getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View detailsView = inflater.inflate(R.layout.fragment_details, container, false);
        setViews(detailsView);
        return detailsView;
    }

    @Override
    public void onResume() {
        super.onResume();
        setViewData();
        if(getActivity() != null){
            detailsViewModel = new ViewModelProvider(getActivity()).get(DetailsViewModel.class);
        }
        loadSatData();

    }

    private void setViews(View view){
        name_TV = (TextView)view.findViewById(R.id.details_name_TV);
        overview_TV = (TextView)view.findViewById(R.id.details_overview_TV);
        phoneNumber_TV = (TextView)view.findViewById(R.id.details_phone_TV);
        email_TV = (TextView)view.findViewById(R.id.details_email_TV);
        website_TV = (TextView)view.findViewById(R.id.details_website_TV);
        grades_TV = (TextView)view.findViewById(R.id.details_grades_TV);
        location_TV = (TextView)view.findViewById(R.id.details_location_TV);
        sat_critical_TV = (TextView)view.findViewById(R.id.details_sat_criticalReading_TV);
        sat_math_TV = (TextView)view.findViewById(R.id.details_sat_math_TV);
        sat_writing_TV = (TextView) view.findViewById(R.id.details_sat_writing_TV);
        sat_title_TV = (TextView)view.findViewById(R.id.details_sat_title_TV);

    }
    private void setViewData(){

        name_TV.setText(school.getSchool_name());
        if (school.getOverview_paragraph() != null){
            overview_TV.setText(school.getOverview_paragraph());
            overview_TV.setVisibility(View.VISIBLE);
        }

        if (school.getPhone_number() != null){
            phoneNumber_TV.setText(school.getPhone_number());
            phoneNumber_TV.setVisibility(View.VISIBLE);
        }

        if (school.getSchool_email() != null){
            email_TV.setText(school.getSchool_email());
            email_TV.setVisibility(View.VISIBLE);
        }

        if(school.getWebsite() != null){
            website_TV.setText(school.getWebsite());
            website_TV.setVisibility(View.VISIBLE);
        }

        if(school.getGrades() != null){
            grades_TV.setText(school.getGrades());
            grades_TV.setVisibility(View.VISIBLE);
        }

        if (school.getLocation() != null){
            location_TV.setText(school.getLocation());
            location_TV.setVisibility(View.VISIBLE);
            location_TV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openMapsLocation();

                }
            });
        }
    }

    //Load Sat data from an observable in DetailsViewModel
    private void loadSatData(){
        if(getActivity() != null){
            detailsViewModel.getSatScoresData().observe(getActivity(), new Observer<SAT>() {
                @Override
                public void onChanged(SAT sat) {
                    schoolSAT = sat;
                    if(getContext() != null){
                        //Set SAT Data
                        sat_writing_TV.setVisibility(View.VISIBLE);
                        sat_math_TV.setVisibility(View.VISIBLE);
                        sat_critical_TV.setVisibility(View.VISIBLE);
                        sat_title_TV.setText(getString(R.string.school_sat_title_details));
                        sat_math_TV.setText(String.format("%s %s", getString(R.string.school_sat_math_details), sat.getSat_math_avg_score()));
                        sat_writing_TV.setText(String.format("%s %s", getString(R.string.school_sat_writing_details), sat.getSat_writing_avg_score()));
                        sat_critical_TV.setText(String.format("%s %s", getString(R.string.school_sat_think_details), sat.getSat_critical_reading_avg_score()));
                    }
                }
            });
            detailsViewModel.getSchoolSatScoreData(school.getDbn(), getContext());

            //Clear any data left
            sat_critical_TV.setText("");
            sat_critical_TV.setVisibility(View.GONE);
            sat_math_TV.setText("");
            sat_math_TV.setVisibility(View.GONE);
            sat_writing_TV.setText("");
            sat_writing_TV.setVisibility(View.GONE);
            sat_title_TV.setText(getString(R.string.school_sat_title_gone_details));

        }
    }

    private void openMapsLocation(){

        if(school.getLocation() != null || school.getLocation().equals("")){
            String getLocationLatLon = school.getLocation()
                    .substring(school.getLocation().indexOf('(') + 1, school.getLocation().indexOf(')'));

            if(getContext() != null){
                detailsViewModel.startGoogleMaps(getContext(), getLocationLatLon);
            }
        }
    }

}