package com.example.a20230301_jonathantorres_nycschools.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a20230301_jonathantorres_nycschools.R;
import com.example.a20230301_jonathantorres_nycschools.model.School;
import com.example.a20230301_jonathantorres_nycschools.ui.DetailsFragment;
import com.example.a20230301_jonathantorres_nycschools.ui.MainActivity;

import java.util.ArrayList;

//Adapter to for the RecycleView on the Search Screen
public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> {

    public ArrayList<School> listOfSchools = new ArrayList<>();

    Context context;

    public SearchAdapter(Context _context, ArrayList<School> _list){
        context = _context;
        listOfSchools = _list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View inflater = LayoutInflater.from(context).inflate(R.layout.search_list_cells, parent, false);
       return new MyViewHolder(inflater);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.name_TV.setText(listOfSchools.get(position).getSchool_name());

        holder.itemView.setOnClickListener(view -> {
            MainActivity mainActivity = (MainActivity)context;
            FragmentManager fragmentManager = mainActivity.getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.search_frame_FL,
                    DetailsFragment.newInstance(listOfSchools.get(position))).addToBackStack("").commit();
        });

        holder.grades_TV.setText(listOfSchools.get(position).getGrades());
    }

    @Override
    public int getItemCount() {
        return listOfSchools.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView name_TV;
        TextView grades_TV;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name_TV = (TextView) itemView.findViewById(R.id.search_cell_name_TV);
            grades_TV = (TextView) itemView.findViewById(R.id.search_cell_grades_TV);

        }
    }

}
